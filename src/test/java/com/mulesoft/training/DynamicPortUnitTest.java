package com.mulesoft.training;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.mule.construct.Flow;
import org.mule.module.http.internal.listener.DefaultHttpListener;
import org.mule.tck.junit4.FunctionalTestCase;
import org.mule.tck.junit4.rule.DynamicPort;

public class DynamicPortUnitTest extends FunctionalTestCase {
	
	@Rule
    public DynamicPort port = new DynamicPort("http.port");

	@Test
	public void DynamicTestPort() throws Exception {
		Flow flow = (Flow) getFlowConstruct("munit-dynamic-portFlow");
		DefaultHttpListener messageSource = (DefaultHttpListener) flow.getMessageSource();
		int httpPort = messageSource.getConfig().getPort();
		
		System.out.println("HTTP Config Port: " + httpPort);
		
		System.out.println("HTTP Junit Dynamic Port:" + port.getNumber());
		
		//runFlow("munit-dynamic-portFlow");
		
		Assert.assertTrue(port.getNumber() == httpPort);
	}

	@Override
	protected String getConfigFile() {
		return "munit-dynamic-port.xml";
	}
}